module Main
  ( main
  ) where

import Prelude hiding
  ( Rational
  )

import System.Environment
  ( getArgs
  )
import System.Exit
  ( exitFailure
  , exitSuccess
  )
import System.IO
  ( hPutStrLn
  , stderr
  )
import Text.Parsec
  ( runParser
  , anyChar
  , between
  , choice
  , many
  , manyTill
  , string
  , try
  , getPosition
  , setPosition
  , unexpected
  , (<?>)
  )

import Mandelbrot.Symbolics
  ( AngledAddress
  , BinaryAngle
  , InternalAddress
  , Kneading
  , Periods
  , Rational
  )
import Mandelbrot.Text
  ( Parser
  , Parse(parser)
  , Plain(plain)
  , HTML(html)
  , LaTeX(latex)
  )

data Item
  = BAn BinaryAngle
  | Knd Kneading
  | AAd AngledAddress
  | IAd InternalAddress
  | Rat Rational
  | Per Periods

instance Plain Item where
  plain (BAn x) = plain x
  plain (Knd x) = plain x
  plain (AAd x) = plain x
  plain (IAd x) = plain x
  plain (Rat x) = plain x
  plain (Per x) = plain x

instance HTML Item where
  html (BAn x) = html x
  html (Knd x) = html x
  html (AAd x) = html x
  html (IAd x) = html x
  html (Rat x) = html x
  html (Per x) = html x

instance LaTeX Item where
  latex (BAn x) = latex x
  latex (Knd x) = latex x
  latex (AAd x) = latex x
  latex (IAd x) = latex x
  latex (Rat x) = latex x
  latex (Per x) = latex x

item :: String -> String -> Parser Item
item open close = choice
  [ t (BAn `fmap` parser) -- starts with ., ends with )
  , t (Knd `fmap` parser) -- starts with 0, 1, *, (, ends with )
  , t (Rat `fmap` parser) -- contains over
  , t (Per `fmap` parser) -- contains p
  , t (AAd `fmap` parser) -- possibly contains arrowright and over
  , t (IAd `fmap` parser) -- possibly contains arrowright, no over
  , do
      _ <- try (string open)
      p <- getPosition
      s <- manyTill anyChar (try (string close))
      setPosition p
      unexpected . show . abbreviate 50 $ s
  ] <?> "item"
  where
    t = try . between (string open) (string close)
    abbreviate n s = case map (splitAt n) (lines s) of
      [(s', [])] -> s'
      (s', _):_ -> s' ++ "..."
      _ -> ""

failure :: String -> IO a
failure s = hPutStrLn stderr ("m-beautify: " ++ s) >> exitFailure

usage :: IO a
usage = do
  hPutStrLn stderr "usage: m-beautify mode open close < input > output"
  hPutStrLn stderr "modes: plain html latex"
  hPutStrLn stderr "example: m-beautify plain '<<<' '>>>' < plain.txt.in > plain.txt"
  exitSuccess

main :: IO ()
main = do
  args <- getArgs
  case args of
    _ | any (`elem` args) (words "-? -h -help --help") -> usage
    [mode, open, close]
      | null open  -> failure "open must not be empty"
      | null close -> failure "close must not be empty"
      | mode `notElem` words "plain html latex" -> failure "mode unknown"
      | otherwise -> do
          let pretty :: Item -> String
              pretty = case mode of
                "plain" -> plain
                "html"  -> html
                "latex" -> latex
                _ -> error "internal error"
              text = choice
                [ Right `fmap` item open close
                , Left `fmap` anyChar
                ]
          input <- getContents
          case runParser (many text) () "" input of
            Right stream -> putStr $ concatMap (either (:[]) pretty) stream
            Left err -> failure $ "parse failure: " ++ show err
    _ -> failure "invalid arguments, try --help"
