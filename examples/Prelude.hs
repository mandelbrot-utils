import Prelude hiding (Rational)
import qualified Prelude as P
import Data.Strict.Tuple (Pair(..))
import Mandelbrot.Numerics as M
import Mandelbrot.Symbolics hiding (concatMap, splitAt, take, drop)
import qualified Mandelbrot.Symbolics as M
import Mandelbrot.Text as M
